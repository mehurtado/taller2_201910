package model.data_structures;

/**
 * Abstract Data Type for a linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface ILinkedList<T extends Comparable<T>> {
	
	public boolean hasNext();

	public Node<T> next();

	public Node<T> getCurrent();

	public void listing();

	public int size();

	public Node<T > get(int position);

	public Node<T> get(T Element);

	public boolean delete(T element);

	public boolean add(T Element);

}
